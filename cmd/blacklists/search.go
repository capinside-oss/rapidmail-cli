package blacklists

import (
	"github.com/spf13/cobra"
	"gitlab.com/capinside/rapidmail-cli/pkg/rapidmail"
)

var (
	searchCmd = &cobra.Command{
		Use:   "search",
		Short: "Search blacklists",
		RunE:  searchRunEFunc,
	}
)

func searchRunEFunc(cmd *cobra.Command, args []string) error {
	params := rapidmail.BlacklistsRequestParams{}

	blacklists, err := rapidmail.SearchAllBlacklists(params)
	if err != nil {
		return err
	}

	return output(blacklists, outputWriter)
}
