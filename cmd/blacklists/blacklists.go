package blacklists

import (
	"encoding/json"
	"fmt"
	"io"
	"os"

	"github.com/spf13/cobra"
	"gitlab.com/capinside/rapidmail-cli/pkg/rapidmail"
)

var (
	outputFormat string
	outputWriter io.Writer = os.Stdout

	blacklistCmd = &cobra.Command{
		Use:   "blacklists",
		Short: "Blacklist commands",
	}
)

func init() {
	blacklistCmd.AddCommand(
		fetchByIDCmd,
		searchCmd,
	)
}

func Command() *cobra.Command {
	return blacklistCmd
}

func output(blacklists []rapidmail.Blacklist, w io.Writer) error {
	switch outputFormat {
	case "wide":
		return outputWide(blacklists, w)
	default:
		return outputJSON(blacklists, w)
	}
}

func outputWide(blacklists []rapidmail.Blacklist, w io.Writer) error {
	return fmt.Errorf("not yet implemented")
}

func outputJSON(blacklists []rapidmail.Blacklist, w io.Writer) error {
	return json.NewEncoder(w).Encode(blacklists)
}
