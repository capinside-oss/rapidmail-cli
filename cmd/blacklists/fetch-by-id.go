package blacklists

import (
	"github.com/spf13/cobra"
	"gitlab.com/capinside/rapidmail-cli/pkg/rapidmail"
)

var (
	fetchByIDCmd = &cobra.Command{
		Use:  "fetch-by-id",
		RunE: fetchByIDRunEFunc,
	}
)

func fetchByIDRunEFunc(cmd *cobra.Command, args []string) error {
	ids, err := rapidmail.StringSliceToIntSlice(args)
	if err != nil {
		return err
	}

	blacklists := []rapidmail.Blacklist{}
	for id := range ids {
		blacklist, err := rapidmail.FetchBlacklistByID(id)
		if err != nil {
			return err
		}
		blacklists = append(blacklists, *blacklist)
	}

	return output(blacklists, outputWriter)
}
