package mailings

import (
	"encoding/json"
	"fmt"
	"os"

	"github.com/spf13/cobra"
	"gitlab.com/capinside/rapidmail-cli/pkg/rapidmail"
)

var (
	mailingsCmd = &cobra.Command{
		Use:   "mailings",
		Short: "Mailing commands",
	}

	outputFormat string
	params       = rapidmail.MailingsRequestParams{}
)

func init() {
	mailingsCmd.AddCommand(
		fetchByIDCmd,
		recipientsCmd,
		searchCmd,
	)
}

func Command() *cobra.Command {
	return mailingsCmd
}

func outputFunc(format string, jsonFunc interface{}, wideFunc interface{}) interface{} {
	switch outputFormat {
	case "wide":
		return wideFunc
	default:
		return jsonFunc
	}
}

func output(mailings []rapidmail.Mailing) error {
	return outputFunc(outputFormat, outputJSON, outputWide).(func([]rapidmail.Mailing) error)(mailings)
}

func outputJSON(mailings []rapidmail.Mailing) error {
	return json.NewEncoder(os.Stdout).Encode(mailings)
}

func outputWide(mailings []rapidmail.Mailing) error {
	return fmt.Errorf("not yet implemented")
}

func outputRecipients(recipients []rapidmail.MailingRecipient) error {
	return outputFunc(outputFormat, outputRecipientsJSON, outputRecipientsWide).(func([]rapidmail.MailingRecipient) error)(recipients)
}

func outputRecipientsJSON(recipients []rapidmail.MailingRecipient) error {
	return json.NewEncoder(os.Stdout).Encode(recipients)
}

func outputRecipientsWide(recipients []rapidmail.MailingRecipient) error {
	return fmt.Errorf("not yet implemented")
}
