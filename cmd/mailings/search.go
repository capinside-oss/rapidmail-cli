package mailings

import (
	"github.com/spf13/cobra"
	"gitlab.com/capinside/rapidmail-cli/pkg/rapidmail"
)

var (
	searchCmd = &cobra.Command{
		Use:   "search",
		Short: "Search mailings",
		RunE:  searchRunEFunc,
	}
)

func init() {
	searchCmd.Flags().StringVarP(&params.Status, "status", "", "", "Filter collection by status")
	searchCmd.Flags().StringVarP(&params.CreatedSince, "created-since", "", "", "Filter collection by created datetime showing all mailings that were created since given datetime. Format: Y-m-d H:i:s")
	searchCmd.Flags().StringVarP(&params.UpdatedSince, "updated-since", "", "", "Filter collection by updated datetime showing all mailings that were updated since given datetime. Format: Y-m-d H:i:s")
}

func searchRunEFunc(cmd *cobra.Command, args []string) error {
	result, err := rapidmail.SearchAllMailings(params)
	if err != nil {
		return err
	}

	return output(result)
}
