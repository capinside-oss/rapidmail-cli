package mailings

import (
	"github.com/spf13/cobra"
	"gitlab.com/capinside/rapidmail-cli/pkg/rapidmail"
)

var (
	fetchByIDCmd = &cobra.Command{
		Use:   "fetch-by-id",
		Short: "Fetch mailing by ID",
		RunE:  fetchByIDRunEFunc,
	}
)

func fetchByIDRunEFunc(cmd *cobra.Command, args []string) error {
	ids, err := rapidmail.StringSliceToIntSlice(args)
	if err != nil {
		return err
	}

	mailings := []rapidmail.Mailing{}
	for id := range ids {
		mailing, err := rapidmail.FetchMailingByID(id)
		if err != nil {
			return err
		}
		mailings = append(mailings, *mailing)
	}

	return output(mailings)
}
