package mailings

import (
	"github.com/spf13/cobra"
	"gitlab.com/capinside/rapidmail-cli/pkg/rapidmail"
)

var (
	recipientsCmd = &cobra.Command{
		Use:   "recipients",
		Short: "Get mailing recipients",
		RunE:  recipientsRunEFunc,
	}
)

func recipientsRunEFunc(cmd *cobra.Command, args []string) error {
	ids, err := rapidmail.StringSliceToIntSlice(args)
	if err != nil {
		return err
	}

	result := []rapidmail.MailingRecipient{}
	for id := range ids {
		response, err := rapidmail.Recipients(id)
		if err != nil {
			return err
		}
		result = append(result, response.Recipients()...)
	}

	return outputRecipients(result)
}
