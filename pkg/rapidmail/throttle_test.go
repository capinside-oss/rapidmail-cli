package rapidmail

import (
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
)

func TestThrottler(t *testing.T) {
	tt := []struct {
		MaxRequestsPerSecond uint
		MinExecutionTime     time.Duration
		Executions           uint
	}{
		{
			MaxRequestsPerSecond: 10,
			Executions:           10,
			MinExecutionTime:     1 * time.Second,
		},
		{
			MaxRequestsPerSecond: 1,
			Executions:           3,
			MinExecutionTime:     3 * time.Second,
		},
	}

	for _, test := range tt {
		throttler := NewThrottler(test.MaxRequestsPerSecond)

		startTime := time.Now()
		for i := 0; i < int(test.Executions); i++ {
			throttler.Wait()
		}
		endTime := time.Now()
		executionTime := endTime.Sub(startTime)
		assert.GreaterOrEqual(t, executionTime, test.MinExecutionTime)
	}
}
