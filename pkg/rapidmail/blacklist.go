package rapidmail

import (
	"encoding/json"
	"fmt"
)

const (
	// BlacklistPath endpoint
	BlacklistPath = "/blacklist"
)

var (
	// Blacklists service global variable
	Blacklists BlacklistsService
)

// BlacklistsService interface
type BlacklistsService interface {
	// FetchByID returns a pointer of Blacklist matching id and nil or nil and an error
	FetchByID(id int) (*Blacklist, error)
	// Search returns a pointer of BlacklistsResponse matching params and nil
	// or nil and an error
	Search(params BlacklistsRequestParams) (*BlacklistsResponse, error)
	// SearchAll returns a slice of Blacklists matching params and nil or nil and an error.
	// Handles pagination by iterating over all pages
	SearchAll(params BlacklistsRequestParams) ([]Blacklist, error)
}

// BlacklistFetchByIDFunc function header
type BlacklistFetchByIDFunc func(int) (*Blacklist, error)

// BlacklistsSearchFunc function header
type BlacklistsSearchFunc func(BlacklistsRequestParams) (*BlacklistsResponse, error)

type blacklistsService struct {
	fetchByIDFunc BlacklistFetchByIDFunc
	searchFunc    BlacklistsSearchFunc
}

// NewBlacklistsService returns an instance of blacklistsService
// that implements the BlacklistsService interface
func NewBlacklistsService(client Client) BlacklistsService {
	return NewBlacklistsServiceWithFunc(
		blacklistFetchByIDFunc(client),
		blacklistsSearchFunc(client),
	)
}

// NewBlacklistsServiceWithFunc returns an instance of blacklistsService
// that implements the BlacklistsService interface and has fetchByIDFunc and searchFunc set.
func NewBlacklistsServiceWithFunc(fetchByIDFunc BlacklistFetchByIDFunc, searchFunc BlacklistsSearchFunc) BlacklistsService {
	return &blacklistsService{
		fetchByIDFunc: fetchByIDFunc,
		searchFunc:    searchFunc,
	}
}

// Blacklist ...
// https://developer.rapidmail.wiki/documentation.html?urls.primaryName=Blacklist#/Blacklist/get_blacklist
type Blacklist struct {
	ID      int    `json:"id,string"`
	Type    string `json:"type"`
	Pattern string `json:"pattern"`
	Source  string `json:"source"`
	Created string `json:"created"`
	Updated string `json:"updated"`
}

// BlacklistsRequestParams data to be passed when querying blacklists
type BlacklistsRequestParams struct {
	APIRequestParams
}

// BlacklistsResponse data
type BlacklistsResponse struct {
	APIResponse
	Embedded map[string][]Blacklist `json:"_embedded"`
}

// Blacklist returns the embedded slice of blacklist
func (br *BlacklistsResponse) Blacklist() []Blacklist {
	return br.Embedded["blacklist"]
}

// Blacklist returns the blacklist with id and nil or nil and an error
func (bs *blacklistsService) FetchByID(id int) (*Blacklist, error) {
	return bs.fetchByIDFunc(id)
}

func blacklistFetchByIDFunc(client Client) BlacklistFetchByIDFunc {
	return func(id int) (*Blacklist, error) {
		path := fmt.Sprintf("%s/%d", BlacklistPath, id)

		data, err := client.Get(path, nil)
		if err != nil {
			return nil, err
		}

		result := Blacklist{}
		return &result, json.Unmarshal(data, &result)
	}
}

// Search returns a pointer of BlacklistsResponse matching params and nil or nil and an error
func (bs *blacklistsService) Search(params BlacklistsRequestParams) (*BlacklistsResponse, error) {
	return bs.searchFunc(params)
}

func blacklistsSearchFunc(client Client) BlacklistsSearchFunc {
	return func(params BlacklistsRequestParams) (*BlacklistsResponse, error) {
		data, err := client.Get(BlacklistPath, params)
		if err != nil {
			return nil, err
		}

		result := &BlacklistsResponse{}
		return result, json.Unmarshal(data, &result)
	}
}

// SearchAll returns a slice of blacklists matching params and nil or nil and an error.
// Handles pagination by searching all pages.
func (bs *blacklistsService) SearchAll(params BlacklistsRequestParams) ([]Blacklist, error) {
	params.Page = 1

	result := []Blacklist{}
	done := false
	for !done {
		resp, err := bs.Search(params)
		if err != nil {
			return nil, err
		}

		result = append(result, resp.Blacklist()...)

		done = resp.Page == resp.PageCount
		params.Page++
	}

	return result, nil
}

// FetchBlacklistByID uses global Blacklists service to fetch the blacklist matching id
// and returns a pointer of Blacklist and nil or nil and an error
func FetchBlacklistByID(id int) (*Blacklist, error) {
	return Blacklists.FetchByID(id)
}

// SearchBlacklists uses global Blacklists service to fetch the blacklists matching params
// and returns a pointer of Blacklist and nil or nil and an error. Does not handle pagination.
func SearchBlacklists(params BlacklistsRequestParams) (*BlacklistsResponse, error) {
	return Blacklists.Search(params)
}

// SearchAllBlacklists uses global Blacklists service to fetch all the blacklists matching params
// and returns a pointer of Blacklist and nil or nil and an error. Handles pagination.
func SearchAllBlacklists(params BlacklistsRequestParams) ([]Blacklist, error) {
	return Blacklists.SearchAll(params)
}
