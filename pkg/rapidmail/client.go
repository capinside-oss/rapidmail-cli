package rapidmail

// docs: https://developer.rapidmail.wiki/documentation.html

import (
	"fmt"
	"io/ioutil"
	"net/http"
	"net/url"
	"os"

	"github.com/google/go-querystring/query"
	"github.com/sirupsen/logrus"
)

const (
	// BaseURLV3 of rapidmail API
	BaseURLV3 = "https://apiv3.emailsys.net"
	// EnvironmentVariableAPIURL name
	EnvironmentVariableAPIURL = "RAPIDMAIL_API_URL"
	// EnvironmentVariableAPIUsername name
	EnvironmentVariableAPIUsername = "RAPIDMAIL_API_USERNAME"
	// EnvironmentVariableAPIPassword name
	EnvironmentVariableAPIPassword = "RAPIDMAIL_API_PASSWORD"
	// Throttling: https://developer.rapidmail.wiki/index.html
	maxRequestsPerSecond uint = 10
	// TimeFormat used by API. See https://gist.github.com/unstppbl/26942512b3ca6a92857c87124445ca0b
	TimeFormat = "2006-01-02 15:04:05"
)

var (
	c Client
)

func init() {
	logrus.SetFormatter(&logrus.JSONFormatter{})
	logrus.SetOutput(os.Stdout)

	baseURL := lookupEnv(EnvironmentVariableAPIURL, BaseURLV3)
	username := lookupEnv(EnvironmentVariableAPIUsername, "")
	password := lookupEnv(EnvironmentVariableAPIPassword, "")

	if err := Init(baseURL, username, password, "info"); err != nil {
		panic(err)
	}
}

// Init initializes the global client
func Init(baseURL string, username string, password string, logLevel string) error {
	cfg, err := NewConfig(baseURL, password, username)
	if err != nil {
		return err
	}

	logLevelParsed, err := logrus.ParseLevel(logLevel)
	if err != nil {
		return fmt.Errorf("parsing log level '%s': %v", logLevel, err)
	}
	logrus.SetLevel(logLevelParsed)

	logrus.WithFields(logrus.Fields{"url": baseURL, "username": username, "log-level": logLevel}).Debug("init")

	c = NewClient(*cfg)

	Blacklists = NewBlacklistsService(c)
	Mailings = NewMailingsService(c)

	return nil
}

// APIError data returned by API in case of errors
type APIError struct {
	Detail string `json:"detail"`
	Status int    `json:"status"`
	Title  string `json:"title"`
	Type   string `json:"type"`
}

// APIResponse data containg meta data for pagination
type APIResponse struct {
	Links      map[string]map[string]string `json:"_links"`
	Page       uint                         `json:"page"`
	PageCount  uint                         `json:"page_count"`
	PageSize   uint                         `json:"page_size"`
	TotalItems uint                         `json:"total_items"`
}

// APIRequestParams presents form data of requests
type APIRequestParams struct {
	Page int `json:"page" url:"page,omitempty"`
}

// Client interface
type Client interface {
	Delete(path string, v interface{}) ([]byte, error)
	Get(path string, v interface{}) ([]byte, error)
	Post(path string, v interface{}) ([]byte, error)
	Put(path string, v interface{}) ([]byte, error)
}

// ClientFunc represents the client function header
type ClientFunc func(string, interface{}) ([]byte, error)

// ClientDeleteFunc function header
type ClientDeleteFunc = ClientFunc

// ClientGetFunc function header
type ClientGetFunc = ClientFunc

// ClientPostFunc function header
type ClientPostFunc = ClientFunc

// ClientPutFunc function header
type ClientPutFunc = ClientFunc

// ClientPatchRequestFunc function header
type ClientPatchRequestFunc func(*http.Request)

// NewClient returns an instance of client that implemenets the Client interface
func NewClient(config Config) Client {
	httpClient := http.DefaultClient
	throttler := NewThrottler(maxRequestsPerSecond)
	patchFunc := setAuthAndHeaderFunc(config.Username, config.Password)

	return NewClientWithFunc(
		config,
		clientDeleteFunc(httpClient, throttler, config.BaseURL, patchFunc),
		clientGetFunc(httpClient, throttler, config.BaseURL, patchFunc),
		clientPostFunc(httpClient, throttler, config.BaseURL, patchFunc),
		clientPutFunc(httpClient, throttler, config.BaseURL, patchFunc),
	)
}

// NewClientWithFunc returns an instance of client that implements
// the Client interface
func NewClientWithFunc(config Config, deleteFunc ClientDeleteFunc, getFunc ClientGetFunc, postFunc ClientPostFunc, putFunc ClientPutFunc) Client {
	return &client{
		deleteFunc: deleteFunc,
		getFunc:    getFunc,
		postFunc:   postFunc,
		putFunc:    putFunc,
	}
}

type client struct {
	deleteFunc ClientDeleteFunc
	getFunc    ClientGetFunc
	postFunc   ClientPostFunc
	putFunc    ClientPutFunc
}

// Delete executes a DELETE request on path using v as header values
// and returns a slice of byte and nil or nil and an error
func (c *client) Delete(path string, v interface{}) ([]byte, error) {
	return c.deleteFunc(path, v)
}

func clientDeleteFunc(httpClient *http.Client, throttler *Throttler, baseURL *url.URL, patchFunc ClientPatchRequestFunc) ClientDeleteFunc {
	return func(path string, v interface{}) ([]byte, error) {
		return request(httpClient, throttler, baseURL, patchFunc, http.MethodDelete, path, v)
	}
}

// Get executes a GET request on path using v as header values
// and returns a slice of byte and nil or nil and an error
func (c *client) Get(path string, v interface{}) ([]byte, error) {
	return c.getFunc(path, v)
}

func clientGetFunc(httpClient *http.Client, throttler *Throttler, baseURL *url.URL, patchFunc ClientPatchRequestFunc) ClientGetFunc {
	return func(path string, v interface{}) ([]byte, error) {
		return request(httpClient, throttler, baseURL, patchFunc, http.MethodGet, path, v)
	}
}

// Post executes a POST request on path using v as header values
// and returns a slice of byte and nil or nil and an error
func (c *client) Post(path string, v interface{}) ([]byte, error) {
	return c.postFunc(path, v)
}

func clientPostFunc(httpClient *http.Client, throttler *Throttler, baseURL *url.URL, patchFunc ClientPatchRequestFunc) ClientPostFunc {
	return func(path string, v interface{}) ([]byte, error) {
		return request(httpClient, throttler, baseURL, patchFunc, http.MethodGet, path, v)
	}
}

// Put executes a PUT request on path using v as header values
// and returns a slice of byte and nil or nil and an error
func (c *client) Put(path string, v interface{}) ([]byte, error) {
	return c.putFunc(path, v)
}

func clientPutFunc(httpClient *http.Client, throttler *Throttler, baseURL *url.URL, patchFunc ClientPatchRequestFunc) ClientPutFunc {
	return func(path string, v interface{}) ([]byte, error) {
		return request(httpClient, throttler, baseURL, patchFunc, http.MethodGet, path, v)
	}
}

func setAuthAndHeaderFunc(username string, password string) func(req *http.Request) {
	return func(req *http.Request) {
		req.SetBasicAuth(username, password)
		req.Header.Set("Accept", "application/json")
		req.Header.Set("Content-Type", "application/json")
	}
}

func request(httpClient *http.Client, throttler *Throttler, baseURL *url.URL, patchRequestFunc ClientPatchRequestFunc, method string, path string, params interface{}) ([]byte, error) {
	ref := &url.URL{Path: path}
	uri := baseURL.ResolveReference(ref)
	req, err := http.NewRequest(method, uri.String(), nil)
	if err != nil {
		return nil, err
	}

	patchRequestFunc(req)

	values, err := query.Values(params)
	if err != nil {
		return nil, err
	}

	req.URL.RawQuery = values.Encode()

	logrus.WithFields(logrus.Fields{"uri": uri.Path, "values": req.URL.RawQuery}).Debug("executing request")

	throttler.Wait()

	resp, err := httpClient.Do(req)
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()

	if resp.StatusCode != http.StatusOK {
		return nil, fmt.Errorf(http.StatusText(resp.StatusCode))
	}

	return ioutil.ReadAll(resp.Body)

}

// Delete executes a DELETE request on path using v as payload
func Delete(path string, v interface{}) ([]byte, error) {
	return c.Delete(path, v)
}

// Get executes a GET request on path using v as payload
func Get(path string, v interface{}) ([]byte, error) {
	return c.Get(path, v)
}

// Put executes a PUT request on path using v as payload
func Put(path string, v interface{}) ([]byte, error) {
	return c.Put(path, v)
}

// Post executes a POST request on path using v as payload
func Post(path string, v interface{}) ([]byte, error) {
	return c.Post(path, v)
}
