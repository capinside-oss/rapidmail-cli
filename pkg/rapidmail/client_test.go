package rapidmail

import (
	"fmt"
	"net/http"
	"net/http/httptest"
	"net/url"
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
)

func setup(path string, statusCode int, body []byte) *httptest.Server {
	mux := http.NewServeMux()
	mux.HandleFunc(path, func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set("Content-Type", "application/json")
		w.WriteHeader(statusCode)
		_, err := w.Write([]byte(body))
		if err != nil {
			panic(err)
		}
	})

	server := httptest.NewServer(mux)

	if err := Init(server.URL, "", "", "debug"); err != nil {
		panic(err)
	}

	return server
}

func TestClient(t *testing.T) {
	assert.NotNil(t, c)
}

func TestNewClient(t *testing.T) {
	config := Config{
		BaseURL: &url.URL{
			Scheme: "https",
			Host:   "capinside.com",
		},
		Password: "t0ps3cr3t",
		Username: "johndoe",
	}

	c := NewClient(config)
	assert.NotNil(t, c)
}

func TestClientDelete(t *testing.T) {
	tt := []struct {
		Path       string
		StatusCode int
		Body       []byte
		Params     interface{}
		Result     []byte
		Error      error
	}{
		{
			Path:       "/error",
			StatusCode: http.StatusInternalServerError,
			Error:      fmt.Errorf(http.StatusText(http.StatusInternalServerError)),
		},
	}

	for _, test := range tt {
		server := setup(test.Path, test.StatusCode, test.Body)

		result, err := Delete(test.Path, test.Params)
		if err != nil {
			if test.Error == nil {
				t.Fatalf("\nunexpected error: %v", err)
			} else {
				if err.Error() != test.Error.Error() {
					t.Fatalf("\nwant: %v\ngot:  %v", test.Error, err)
				}
			}
		}
		if !assert.Equal(t, test.Result, result) {
			t.Fatalf("\nwant: %v\ngot:  %v", test.Result, result)
		}
		server.Close()
	}
}

func TestClientGet(t *testing.T) {
	tt := []struct {
		Path       string
		StatusCode int
		Body       []byte
		Params     interface{}
		Result     []byte
		Error      error
	}{
		{
			Path:       "/error",
			StatusCode: http.StatusInternalServerError,
			Error:      fmt.Errorf(http.StatusText(http.StatusInternalServerError)),
		},
	}

	for _, test := range tt {
		server := setup(test.Path, test.StatusCode, test.Body)

		result, err := Get(test.Path, test.Params)
		if err != nil {
			if test.Error == nil {
				t.Fatalf("\nunexpected error: %v", err)
			} else {
				if err.Error() != test.Error.Error() {
					t.Fatalf("\nwant: %v\ngot:  %v", test.Error, err)
				}
			}
		}
		if !assert.Equal(t, test.Result, result) {
			t.Fatalf("\nwant: %v\ngot:  %v", test.Result, result)
		}
		server.Close()
	}
}

func TestClientPost(t *testing.T) {
	tt := []struct {
		Path       string
		StatusCode int
		Body       []byte
		Params     interface{}
		Result     []byte
		Error      error
	}{
		{
			Path:       "/error",
			StatusCode: http.StatusInternalServerError,
			Error:      fmt.Errorf(http.StatusText(http.StatusInternalServerError)),
		},
	}

	for _, test := range tt {
		server := setup(test.Path, test.StatusCode, test.Body)

		result, err := Post(test.Path, test.Params)
		if err != nil {
			if test.Error == nil {
				t.Fatalf("\nunexpected error: %v", err)
			} else {
				if err.Error() != test.Error.Error() {
					t.Fatalf("\nwant: %v\ngot:  %v", test.Error, err)
				}
			}
		}
		if !assert.Equal(t, test.Result, result) {
			t.Fatalf("\nwant: %v\ngot:  %v", test.Result, result)
		}
		server.Close()
	}
}

func TestClientPut(t *testing.T) {
	tt := []struct {
		Path       string
		StatusCode int
		Body       []byte
		Params     interface{}
		Result     []byte
		Error      error
	}{
		{
			Path:       "/error",
			StatusCode: http.StatusInternalServerError,
			Error:      fmt.Errorf(http.StatusText(http.StatusInternalServerError)),
		},
		{
			Path:       "/put",
			StatusCode: http.StatusOK,
			Body:       []byte("{\"test\":\"data\"}"),
			Result:     []byte("{\"test\":\"data\"}"),
		},
	}

	for _, test := range tt {
		server := setup(test.Path, test.StatusCode, test.Body)

		result, err := Put(test.Path, test.Params)
		if err != nil {
			if test.Error == nil {
				t.Fatalf("\nunexpected error: %v", err)
			} else {
				if err.Error() != test.Error.Error() {
					t.Fatalf("\nwant: %v\ngot:  %v", test.Error, err)
				}
			}
		}
		if !assert.Equal(t, test.Result, result) {
			t.Fatalf("\nwant: %v\ngot:  %v", test.Result, result)
		}
		server.Close()
	}
}

func TestSetAuthAndHeaderFunc(t *testing.T) {
	tt := []struct {
		Username string
		Password string
		Request  *http.Request
		Result   *http.Request
	}{
		{
			Username: "test username",
			Password: "test password",
			Request: &http.Request{
				Header: make(http.Header),
			},
			Result: &http.Request{
				Header: map[string][]string{
					"Accept":        {"application/json"},
					"Authorization": {"Basic dGVzdCB1c2VybmFtZTp0ZXN0IHBhc3N3b3Jk"},
					"Content-Type":  {"application/json"},
				},
			},
		},
	}

	for _, test := range tt {
		f := setAuthAndHeaderFunc(test.Username, test.Password)
		require.NotNil(t, f)
		f(test.Request)

		if !assert.Equal(t, test.Result, test.Request) {
			t.Fatalf("\nwant: %v\ngot:  %v", test.Result, test.Request)
		}
	}
}
