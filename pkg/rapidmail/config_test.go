package rapidmail

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
)

func TestNewConfig(t *testing.T) {
	tt := []struct {
		RawURL   string
		Password string
		Username string
		Error    error
	}{
		{
			RawURL:   BaseURLV3,
			Username: "test-user",
			Password: "test-password",
		},
	}

	for _, test := range tt {
		config, err := NewConfig(test.RawURL, test.Password, test.Username)
		if err != nil {
			if test.Error == nil {
				t.Fatalf("\nunexpected error: %v", err)
			} else {
				if err.Error() != test.Error.Error() {
					t.Fatalf("\nwant: %v\ngot:  %v", test.Error, err)
				}
			}
		}
		require.NotNil(t, config)
		assert.Equal(t, test.RawURL, config.BaseURL.String())
		assert.Equal(t, test.Password, config.Password)
		assert.Equal(t, test.Username, config.Username)
	}
}
