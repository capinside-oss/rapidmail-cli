package rapidmail

import (
	"os"
	"strconv"
)

func lookupEnv(key string, defaultValue string) string {
	value, found := os.LookupEnv(key)
	if found {
		return value
	}
	return defaultValue
}

// StringSliceToIntSlice converts s into a slice of int and returns the slice of int's and nil or nil and an error
func StringSliceToIntSlice(s []string) ([]int, error) {
	result := make([]int, len(s))
	for i, v := range s {
		id, err := strconv.Atoi(v)
		if err != nil {
			return nil, err
		}

		result[i] = id
	}

	return result, nil
}
