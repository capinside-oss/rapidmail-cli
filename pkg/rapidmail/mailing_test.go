package rapidmail

import (
	"fmt"
	"net/http"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestMailingsServiceFetchByID(t *testing.T) {
	tt := []struct {
		ID         int
		Body       []byte
		StatusCode int
		Result     *Mailing
		Error      error
	}{
		{
			ID:         -1,
			Body:       []byte(``),
			StatusCode: http.StatusInternalServerError,
			Result:     nil,
			Error:      fmt.Errorf(http.StatusText(http.StatusInternalServerError)),
		},
		{
			ID:         12345,
			Body:       []byte(`{"id":"12345","recipients":"1","attachments":"1"}`),
			StatusCode: http.StatusOK,
			Result: &Mailing{
				ID:          12345,
				Recipients:  1,
				Attachments: 1,
			},
			Error: nil,
		},
	}

	for _, test := range tt {
		server := setup(fmt.Sprintf("%s/%d", MailingsPath, test.ID), test.StatusCode, test.Body)

		result, err := FetchMailingByID(test.ID)
		if err != nil {
			if test.Error == nil {
				t.Fatalf("unexpected error: %v", err)
			} else {
				if err.Error() != test.Error.Error() {
					t.Fatalf("\nwant: %v\ngot:  %v", test.Error, err)
				}
			}
		}
		if !assert.Equal(t, test.Result, result) {
			t.Fatalf("\nwant: %v\ngot:  %v", test.Result, result)
		}

		server.Close()
	}
}

func TestMailingsServiceSearchAll(t *testing.T) {
	tt := []struct {
		Params     MailingsRequestParams
		Body       []byte
		StatusCode int
		Result     []Mailing
		Error      error
	}{
		{
			Params:     MailingsRequestParams{},
			Body:       []byte(``),
			StatusCode: http.StatusInternalServerError,
			Result:     nil,
			Error:      fmt.Errorf(http.StatusText(http.StatusInternalServerError)),
		},
		{
			Params:     MailingsRequestParams{},
			Body:       []byte(`{"page":0,"page_count":0,"_embedded":{"mailings":[]}}`),
			StatusCode: http.StatusOK,
			Result:     []Mailing{},
			Error:      nil,
		},
		{
			Params:     MailingsRequestParams{},
			Body:       []byte(`{"_embedded":{"mailings":[{"id":"12345","recipients":"1","attachments":"1"}]}}`),
			StatusCode: http.StatusOK,
			Result: []Mailing{
				{
					ID:          12345,
					Recipients:  1,
					Attachments: 1,
				},
			},
			Error: nil,
		},
	}

	for _, test := range tt {
		server := setup(MailingsPath, test.StatusCode, test.Body)

		result, err := SearchAllMailings(test.Params)
		if err != nil {
			if test.Error == nil {
				t.Fatalf("unexpected error: %v", err)
			} else {
				if err.Error() != test.Error.Error() {
					t.Fatalf("\nwant: %v\ngot:  %v", test.Error, err)
				}
			}
		}
		if !assert.Equal(t, test.Result, result) {
			t.Fatalf("\nwant: %v\ngot:  %v", test.Result, result)
		}

		server.Close()
	}
}

func TestMailingsServiceRecipients(t *testing.T) {
	tt := []struct {
		ID         int
		Params     APIRequestParams
		Body       []byte
		StatusCode int
		Result     *MailingRecipientsResponse
		Error      error
	}{
		{
			ID:         -1,
			Body:       []byte(``),
			StatusCode: http.StatusInternalServerError,
			Result:     nil,
			Error:      fmt.Errorf(http.StatusText(http.StatusInternalServerError)),
		},
		{
			ID:         1,
			Params:     APIRequestParams{},
			Body:       []byte(`{"_embedded":{"mailingrecipients":[]}}`),
			StatusCode: http.StatusOK,
			Result: &MailingRecipientsResponse{
				Embedded: map[string][]MailingRecipient{
					"mailingrecipients": {},
				},
			},
			Error: nil,
		},
	}

	for _, test := range tt {
		server := setup(fmt.Sprintf("%s/%d/stats/activity", MailingsPath, test.ID), test.StatusCode, test.Body)

		result, err := Recipients(test.ID)
		if err != nil {
			if test.Error == nil {
				t.Fatalf("unexpected error: %v", err)
			} else {
				if err.Error() != test.Error.Error() {
					t.Fatalf("\nwant: %v\ngot:  %v", test.Error, err)
				}
			}
		}
		if !assert.Equal(t, test.Result, result) {
			t.Fatalf("\nwant: %v\ngot:  %v", test.Result, result)
		}

		server.Close()
	}
}
