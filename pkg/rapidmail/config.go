package rapidmail

import "net/url"

// Config data
type Config struct {
	BaseURL  *url.URL
	Password string
	Username string
}

// NewConfig returns an instance of Config and nil or nil and an error
// if rawURL can't be parsed
func NewConfig(rawURL string, password string, username string) (*Config, error) {
	baseURL, err := url.Parse(rawURL)
	if err != nil {
		return nil, err
	}

	config := &Config{
		BaseURL:  baseURL,
		Password: password,
		Username: username,
	}

	return config, nil
}
