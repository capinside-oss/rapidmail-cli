package rapidmail

import (
	"time"
)

// Throttler ensure no more than maxRequestsPerSecond are executed
type Throttler struct {
	lastExecutionTime time.Time
	sleepDuration     time.Duration
}

// NewThrottler returns an instance of Throttler with maxRequestsPerSecond set
func NewThrottler(maxRequestsPerSecond uint) *Throttler {
	return &Throttler{
		lastExecutionTime: time.Now(),
		sleepDuration:     time.Duration(1000/maxRequestsPerSecond) * time.Millisecond,
	}
}

// Wait waits until enough time has passed by
func (t *Throttler) Wait() {
	durationSinceLastExecution := time.Since(t.lastExecutionTime)

	if durationSinceLastExecution < t.sleepDuration {
		time.Sleep(t.sleepDuration - durationSinceLastExecution)
	}

	t.lastExecutionTime = time.Now()
}
