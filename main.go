package main

import (
	"fmt"
	"os"

	"gitlab.com/capinside/rapidmail-cli/cmd"
)

func main() {
	if err := cmd.Execute(); err != nil {
		fmt.Errorf("error: %v", err)
		os.Exit(1)
	}
}
